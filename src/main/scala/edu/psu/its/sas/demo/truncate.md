# Assignment

Write a function, truncate(string, length), that truncates a string to the length.

- If the string is truncated, it should end with "..."
- If the string length is less than the length parameter, it should return original string

# Example

    str = "this is a very long string blah blah blah"
    truncate(str, 22) => "this is a very long..."

    truncate("hello", 200) => "hello"
    truncate("hello", 4) => "h...")
