package edu.psu.its.sas.demo

import org.scalacheck.Gen

import scala.util.Random

object Generators {

  def psuAccessId = for {
    first <- Gen.alphaLowerChar
    middle <- Gen.alphaLowerChar
    last <- Gen.alphaLowerChar
    hasNum <- Gen.frequency((1, false), (9, true))
    num <- Gen.choose(1, 9999)
  } yield {
    val base = s"$first$middle$last"
    if (hasNum) s"$base$num" else base
  }

}
