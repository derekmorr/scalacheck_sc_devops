package edu.psu.its.sas.demo

import edu.psu.its.sas.demo.Demo.absValue
import org.scalatest.{WordSpec, MustMatchers}

class AbsValueUnitTests extends WordSpec with MustMatchers {
  
  "AbsValue" must {
    "return 1 for 1" in {
      absValue(1) mustEqual 1
    }

    "return 0 for 0" in {
      absValue(0) mustEqual 0
    }

    "return Int.MaxValue for Int.MaxValue" in {
      absValue(Int.MaxValue) mustEqual Int.MaxValue
    }

    "be non-negative" in {
      import scala.util.Random
      for (i <- 0 to 10000) {
        val randomNumber = Random.nextInt
        val output = absValue(randomNumber)
        output must be >= 0
      }
    }
  }
}
