package edu.psu.its.sas.demo

import Generators.psuAccessId
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{WordSpec, MustMatchers}

class GeneratorDemoTest extends WordSpec with MustMatchers with GeneratorDrivenPropertyChecks {

  "AccessID generator" must {

    "generate random ids" in {
      forAll(psuAccessId) { accessId: String =>
        println(accessId)
      }
    }

  }

}
