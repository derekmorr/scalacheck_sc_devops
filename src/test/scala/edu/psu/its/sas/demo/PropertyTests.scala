package edu.psu.its.sas.demo

import java.util.zip.Deflater

import edu.psu.its.sas.demo.Compressor._
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{MustMatchers, WordSpec}

/**
 * Some sample property tests.
 * Inspired by http://fsharpforfunandprofit.com/posts/property-based-testing-2/
 */
class PropertyTests extends WordSpec with MustMatchers with GeneratorDrivenPropertyChecks {

  "compress" must {
    // a sanity check
    "never make output larger than input" in {
      forAll { str: String =>
        // we have to use NO_COMPRESSION to pick up the DEFLATE header
        val uncompressed = compress(str, Deflater.NO_COMPRESSION).length
        val compressed = compress(str, Deflater.DEFAULT_COMPRESSION).length

        compressed must be <= uncompressed
      }
    }

    // a relation test
    "max compression must be at least as good as normal" in {
      forAll { str: String =>
        val maxCompressed = compress(str, Deflater.BEST_COMPRESSION).length
        val normalcompressed = compress(str, Deflater.DEFAULT_COMPRESSION).length

        maxCompressed must be <= normalcompressed
      }
    }
  }

  "base64" must {
    // round trip test
    "be reversible" in {
      forAll { str: String =>
        base64Decode(base64Encode(str)) mustEqual str
      }
    }
  }

  "absolute value" must {
    // sanity check w/ guard
    "always be non-negative" in {
      forAll { num: Int =>
        // see https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html#abs-int-
        whenever(num != Int.MinValue) {
          Math.abs(num) must be >= 0
        }
      }
    }
  }

  "Lists" must {
    // once it's done, it's done. idempotency.
    "not remove elements from distinct" in {
      forAll { list: List[Int] =>
        val distinct = list.distinct
        distinct.distinct must contain theSameElementsAs distinct
      }
    }

    // "some things never change"
    "not lose elements when sorting" in {
      forAll { list: List[Int] =>
        val sorted = list.sorted
        sorted must contain theSameElementsAs list
      }
    }
  }

}
