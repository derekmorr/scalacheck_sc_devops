package edu.psu.its.sas.demo

import org.scalacheck.Gen.choose
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{WordSpec, MustMatchers}

class TruncateTest extends WordSpec with MustMatchers with GeneratorDrivenPropertyChecks {

  def truncate(str: String, length: Int): String = {
    if (str.length < length)
      str
    else if (length < 3 || str.length < 3)
      ""
    else
      str.take(str.length-3) + "..."
  }

  "Truncate output" must {
    "be <= length of input" in {
      forAll { (str: String, len: Int) =>
        truncate(str, len).length must be <= str.length
      }
    }

    "be <= length of truncate length" in {
      forAll { (str: String, len: Int) =>
        if (len < 3)
          truncate(str, len).length mustEqual 0
        else
          truncate(str, len).length must be <= len
      }
    }

    "be the input if truncate length < string length" in {
      forAll { (str: String, len: Int) =>
        whenever(str.length < len) { truncate(str, len) must equal (str) }
      }
    }

    "start with the input" in {
      forAll { (str: String, len: Int) =>
        whenever (len > 3) { truncate(str, len) must startWith (str) }
      }
    }

    "output must end with ... for long output" in {
      forAll { str: String =>
        whenever(str.length > 3) {
          forAll(choose(3, str.length-1)) { len: Int =>
            truncate(str, len) must endWith ("...")
          }
        }
      }
    }
  }
}
