package edu.psu.its.sas.demo

import edu.psu.its.sas.demo.Demo.absValue
import org.scalacheck.Arbitrary._
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{MustMatchers, WordSpec}

class AbsValuePropertyTest extends WordSpec with MustMatchers with GeneratorDrivenPropertyChecks {

  "AbsValue" must {
    "always return non-negative numbers" in {
      forAll { n: Int =>
        whenever (n != Int.MinValue) {
          absValue(n) must be >= 0
        }
      }
    }

    "return the value or its negation" in {
      forAll { n: Int =>
        whenever(n != Int.MinValue) {
          absValue(n) must (equal(n) or equal(-n))
        }
      }
    }
  }

  "brokenAbsValue" must {
    "always return non-negative numbers" ignore {
      forAll { n: Int =>
        absValue(n) must be >= 0
      }
    }
  }
}
