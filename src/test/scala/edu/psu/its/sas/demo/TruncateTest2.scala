package edu.psu.its.sas.demo

import org.scalacheck.Arbitrary._
import org.scalacheck.Gen
import org.scalatest.{MustMatchers, WordSpec}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

/**
 * Tests for truncate
 */
class TruncateTest2 extends WordSpec with MustMatchers with GeneratorDrivenPropertyChecks {

  // an obviously wrong implementation just to get us started
  def truncate(str: String, length: Int) = {
    if (length <= 3)
      ""
    else if (str.length > length)
      str.take(length - 3) + "..."
    else
      str
  }
  
  "truncate" must {

    "does not truncate short strings" in {
      forAll { (str: String, len: Int) =>
        if (str.length <= len) {
          truncate(str, len) mustEqual str
        }
      }
    }

    "long strings must end w/ ..." in {
      forAll(arbitrary[String], Gen.posNum[Int]) { (str: String, len: Int) =>
        if (len < str.length  && len >= 4) {
          truncate(str, len) must endWith ("...")
        }
      }
    }

    "return empty string for 0 length" in {
      forAll { str: String =>
        truncate(str, 0) mustEqual ""
      }
    }

    "negative len must return empty string" in {
      forAll(arbitrary[String], Gen.negNum[Int]) { (str: String, len: Int) =>
        truncate(str, len) mustEqual ""
      }
    }
  }
}












