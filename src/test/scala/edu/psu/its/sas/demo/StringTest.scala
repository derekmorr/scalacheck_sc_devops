package edu.psu.its.sas.demo

import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{MustMatchers, WordSpec}

/**
 * Property based tests for strings.
 */
class StringTest extends WordSpec with MustMatchers with GeneratorDrivenPropertyChecks {

  "Strings" must {
    "lower must be the same as upper-then-lower" ignore {
      forAll { string: String =>
        val lower = string.toLowerCase
        val lower2 = string.toUpperCase.toLowerCase

        lower mustEqual lower2
      }
    }

    "concat two values properly" in {
      forAll { (strA: String, strB: String) =>
        strA + strB must startWith (strA)
      }
    }

    "concat" must {
      "preserve length" ignore {
        forAll { (strA: String, strB: String) =>
          val concatLen = (strA + strB).length
          concatLen must be > strA.length
          concatLen must be > strB.length
        }
      }
    }
  }
}
