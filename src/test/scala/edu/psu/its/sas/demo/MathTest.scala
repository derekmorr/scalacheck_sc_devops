package edu.psu.its.sas.demo

import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{MustMatchers, WordSpec}

/**
 * Some math tests
 */
class MathTest extends WordSpec with MustMatchers with GeneratorDrivenPropertyChecks {

  "Multiply and divide" ignore {
    forAll { (x: Int, y: Int) =>
      (x * y) / y mustEqual x
    }
  }
  
}
