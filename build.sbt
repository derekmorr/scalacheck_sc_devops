name := "scalacheck_demo"

version := "1.0"

scalaVersion := "2.11.7"

scalacOptions ++= Seq(
  "-feature", "-unchecked", "-deprecation", "-Xcheckinit", "-Xlint",
  "-Xlint:-missing-interpolator,_", "-Xfatal-warnings", "-g:line",
  "-Ywarn-dead-code")

// I'm using a pre-release version of scalatest to pick up the
// allElementsOf matcher used in ListTest.
libraryDependencies ++= {
  Seq(
//    "org.scalatest" %% "scalatest" % "2.2.5" % Test,
    "org.scalatest" %% "scalatest" % "3.0.0-SNAP5",
    "org.scalacheck" %% "scalacheck" % "1.12.4" % Test,
    "org.pegdown" % "pegdown" % "1.5.0"  % Test
  )
}

(testOptions in Test) += Tests.Argument(TestFrameworks.ScalaTest, "-oD")

