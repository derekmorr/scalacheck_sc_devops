# Property based testing basics

This project has some sample property based tests.

# Prerequisites

You will need [Java installed](http://java.oracle.com/). The app will download any additional dependencies.

# Running the app:

From a shell prompt, run

    ./sbt

This will launch sbt, the scala build tool. SBT has its own shell.
At the sbt prompt, run all the tests, run `test:compile`

    $ ./sbt
    Downloading sbt launcher for 0.13.9:
      From  http://repo.typesafe.com/typesafe/ivy-releases/org.scala-sbt/sbt-launch/0.13.9/sbt-launch.jar
        To  /home/derek/.sbt/launchers/0.13.9/sbt-launch.jar
    [info] Loading project definition from /home/derek/scalacheck/project
    [info] Set current project to scalacheck_demo (in build file:/home/derek/scalacheck/)
    > test:compile

The first time you run sbt it will take forever to startup and download the internet.
Go get a beverage of choice. Subsequent runs will go much faster.

To run all of the tests, type `test`

To run only a specific test, use testOnly:

  > testOnly edu.psu.its.sas.demo.AbsValueUnitTests

Once the tests are compiled, you can just type `testOnly e<tab>` to save some typing.

To exit sbt, type `exit`

# Structure

- PropertyTests.scala has examples of different types of property-based tests.
- AbsValueUnitTests shows unit tests for a broken absolute value function. It doens't catch the bug.
- AbsValuePropertyTests shows property tests for absolute value.
- Generators has an example of a custom generator that makes PSU access ids.
- GeneratorDemoTest would print out access ids. The test is skipped. Change "ignore" to "in" on line 11 to run it.
- ListTest shows some sanity checks for the Scala List class.

# Resources

- [Choosing properties for property-based testing](http://fsharpforfunandprofit.com/posts/property-based-testing-2/)
- [Property-Based Testing for Better code](https://www.youtube.com/watch?v=shngiiBfD80)
- [I Dream of Gen'nning'](https://www.youtube.com/watch?v=lgyGFG6hBa0)
- [Testing the Hard Stuff and Staying Sane](https://www.youtube.com/watch?v=zi0rHwfiX1Q)
- [ScalaCheck web site](https://www.scalacheck.org/)
- [ScalaCheck Book](http://www.artima.com/shop/scalacheck)
